/*

MongoDB Aggregation

	syntax:
	db.collection.aggregate(
		[
			{stage1},
			{stage2},
			{stage3}
		]
	)

PIPELINE STAGES


*/


db.fruits.insertMany(
	[
		{
			"name": "Apple",
			"color": "Red",
			"stock": 20,
			"price": 40,
			"onSale": true,
			"origin": ["Philippines", "US"]
		},
		{
			"name": "Banana",
			"color": "Yellow",
			"stock": 15,
			"price": 20,
			"onSale": true,
			"origin": ["Philippines", "Ecuador"]
		},
		{
			"name": "Kiwi",
			"color": "Green",
			"stock": 25,
			"price": 50,
			"onSale": true,
			"origin": ["US", "China"]
		}, 
		{
			"name": "Mango",
			"color": "Yellow",
			"stock": 10,
			"price": 120,
			"onSale": false,
			"origin": ["Philippines", "India"]
		}
	]
	)



db.fruits.aggregate([
	{ $match: { onSale: true } }
	])

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: {
		_id : "$_id",
		totalStock: {$sum: "$stock" }
	}}
	])


// pipeline stages: match, project, sort(1/-1)
db.fruits.aggregate([
	{$match: {stock: {$gt:10}}},
	{$project: {_id: 0, name:1, stock:1}},{$sort: {stock:1}},

	])

// match, count

db.fruits.aggregate([
    {$match:{price:{$gte:50}}},
    {$count:"fruitCount"}
])

// unwind, group

db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group:{
		_id: "$origin", kinds: {$sum:1}
	}}
	])
